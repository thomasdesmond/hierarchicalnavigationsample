﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavSample
{
    public partial class HierarchicalNavSamplePage : ContentPage
    {
        public HierarchicalNavSamplePage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HierarchicalNavSamplePage)}:  ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        void OnEternalYouthTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnEternalYouthTapped)}");
            Navigation.PushAsync(new EternalYouthPage());
        }

        void OnDirectionsTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDirectionsTapped)}");
            Navigation.PushAsync(new HappyvillePage());
        }

        void OnLunchTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnLunchTapped)}");
            Navigation.PushAsync(new FreeLunchPage());
        }
    }
}
