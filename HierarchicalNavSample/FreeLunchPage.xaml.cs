﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavSample
{
    public partial class FreeLunchPage : ContentPage
    {
        public FreeLunchPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(FreeLunchPage)}:  ctor");
            InitializeComponent();
        }
    }
}
