﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavSample
{
    public partial class HappyvillePage : ContentPage
    {
        public HappyvillePage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HappyvillePage)}:  ctor");
            InitializeComponent();
        }
    }
}
