﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavSample
{
    public partial class EternalYouthPage : ContentPage
    {
        public EternalYouthPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(EternalYouthPage)}:  ctor");
            InitializeComponent();
        }
    }
}
